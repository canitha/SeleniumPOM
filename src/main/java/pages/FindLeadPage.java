package pages;

import org.openqa.selenium.WebElement;
import wdMethods.ProjectMethods;

public class FindLeadPage  extends ProjectMethods{

	public FindLeadPage typeFindFirstName(String data) {
		WebElement elefirstName = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(elefirstName, data);
		return this;
	}
	
	public FindLeadPage typeFindLastName(String data) {
		WebElement elelastName = locateElement("xpath", "(//input[@name='lastName'])[3]");
		type(elelastName, data);
		return this;
	}
	
	public FindLeadPage clickFindLeads() throws InterruptedException {
		WebElement eleFindBtn= locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindBtn);
		Thread.sleep(3000);
		return this;
	}

	public ViewLeadPage clickFirstLeads() {
		WebElement eleFirstId = locateElement("xpath", "(//div[@class='x-grid3-scroller']//a)[1]");
		clickWithNoSnap(eleFirstId);
		return new ViewLeadPage();
	}
	
}
