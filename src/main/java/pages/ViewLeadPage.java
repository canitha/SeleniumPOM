package pages;

import org.openqa.selenium.WebElement;

import cucumber.api.java.en.Then;
import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{
	@Then("Verify Lead is created succesfully")
	public ViewLeadPage verifyViewLead()
	{
		System.out.println("Create Lead is successful");
		return this;
		
	}
	public EditLeadPage clickEditLead() {
		WebElement eleEdit = locateElement("linktext", "Edit");
		click(eleEdit);
		return new EditLeadPage();
	}
	
}
