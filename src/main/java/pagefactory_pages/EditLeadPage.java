package pagefactory_pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods{

	public EditLeadPage()
	{
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="updateLeadForm_firstName")
	WebElement eleEditFirst;
	@FindBy(id="updateLeadForm_lastName")
	WebElement eleEditLast;
	@FindBy(xpath="//input[@value='Update']")
	WebElement eleUpdateBtn;


	public EditLeadPage typeFirstName(String data) {
		eleEditFirst.clear();
		type(eleEditFirst, data);
		return this;
	}
	public EditLeadPage typeLastName(String data) {
		eleEditLast.clear();
		type(eleEditLast, data);
		return this;
	}

	public ViewLeadPage clickUpdateLead() {
		click(eleUpdateBtn);
		return new ViewLeadPage(); 
	}

}
