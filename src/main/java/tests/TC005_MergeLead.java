package tests;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;

//import org.junit.Test;

import org.testng.annotations.Test;

import pagefactory_pages.MyHomePage;
import pagefactory_pages.WindowFindLeadPage;
import wdMethods.ProjectMethods;

public class TC005_MergeLead extends ProjectMethods {

	@BeforeClass(/*groups="common"*/)
	public void setData() {
		testCaseName = "TC005_MergeLead";
		testCaseDescription ="Merge two leads";
		category = "Smoke";
		author= "Babu";
		dataSheetName="TC005";
	}
	@Test(dataProvider="fetchData")
	public  void mergeLead(String fromLead, String toLead,String errorMag) throws InterruptedException   {
		new MyHomePage()
		.clickLeads()
		.clickMergeLead()
		.clickFirstLookup()
		.typeFirstName(fromLead)
		.clickFindLeadBtn()
		.clickFirstId();

		/*new WindowFindLeadPage()
		.typeFirstName(fromLead)
		.clickFindLeadBtn()
		.clickFirstId();
		switchToWindow(0);
		.clickSecondLookup()
		.typeFirstName(toLead)


	/*	
		acceptAlert();
		click(locateElement("linktext", "Find Leads"));
		type(locateElement("xpath", "//input[@name='id']"),leadId);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		Thread.sleep(1000);
		WebElement eleVerify = locateElement("class", "x-paging-info");
		verifyExactText(eleVerify, errorMag);


	}
*/
}
}

